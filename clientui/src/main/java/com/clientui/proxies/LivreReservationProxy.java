package com.clientui.proxies;


import com.clientui.beans.EmpruntBean;
import com.clientui.beans.LivreBean;
import com.clientui.beans.MembreBean;
import feign.Param;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
@Service("proxyLivreReservation")
@FeignClient(name = "zuul-server")
@RibbonClient(name="livrereservation")
//@FeignClient(name="livre-reservation" , url="localhost:8084")
public interface LivreReservationProxy {


    @GetMapping("/livrereservation/api/listeEmprunts")
    List<EmpruntBean> listeEmprunts();

    @GetMapping("/livrereservation/api/listeEmpruntsEncours")
    public List<EmpruntBean> listeEmpruntsEncours(@RequestParam(value = "username", required=false) String username);

    @GetMapping("/livrereservation/api/listeEmpruntsEncours/{username}")
    List<EmpruntBean> listeEmpruntsEncoursUsername(@PathVariable( "username") String username);


    @GetMapping("/livrereservation/api/Livre/listeLivres")
    List<LivreBean> listeLivres();

    @GetMapping("/livrereservation/api/Livre/listeLivres/Auteur")
    public List<LivreBean> listeLivresAuteur(@RequestParam("auteur") String auteur);


    @GetMapping("/livrereservation/api/Livre/listeLivres/Titre")
    public List<LivreBean> listeLivresTitre(@RequestParam("titre") String titre);


    @GetMapping(value="/livrereservation/api/Livre/{id}")
    Optional<LivreBean> detailLivre(@PathVariable int id);



    @PostMapping("/livrereservation/api/Livre/listeLivres/recherche")
    List<LivreBean> listeLivresAuteur(@RequestParam(value = "auteur", required = false) String auteur, @RequestParam(value = "titre", required = false) String titre);


    @PutMapping("/livrereservation/api/listeEmpruntsEncours/upade-emprunt/{empruntId}")
    EmpruntBean prolongerEmprunt(@PathVariable("empruntId") long empruntId);


}
