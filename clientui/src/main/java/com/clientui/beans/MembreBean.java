package com.clientui.beans;

import com.clientui.roles.RoleEnum;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
public class MembreBean implements UserDetails {

    @Id
    private Long id;

    private String userName;

    private String password;

    private String nom;

    private String prenom;

    private String email;

    private String numero_telephone;

    private boolean accountNonExpired = true;

    private boolean accountNonLocked = true;

    private boolean credentialsNonExpired = true;

    private boolean enabled = true;

    private Collection<RoleEnum> roles;


    public MembreBean() {
        this.accountNonExpired = true;
        this.accountNonLocked = true;
        this.credentialsNonExpired = true;
        this.enabled = true;
        this.roles = Collections.singletonList(RoleEnum.USER);
    }


    public   MembreBean(long id, String userName, String password, String nom,String prenom, String email,  String numero_telephone,Collection<RoleEnum> roles
    ){
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.numero_telephone = numero_telephone;
        this.roles = roles;

    }




    public boolean isAccountNonExpired() {
        return  accountNonExpired;
    }


    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public boolean isEnabled() {
        return enabled;
    }



    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String roles = StringUtils.collectionToCommaDelimitedString(getRoles().stream()
                .map(Enum::name).collect(Collectors.toList()));
        return AuthorityUtils.commaSeparatedStringToAuthorityList(roles);

    }

    @OneToMany(mappedBy = "membre", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<EmpruntBean> empruntBean = new HashSet<>();


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }



    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }


    public void setId(Long id) {
        this.id = id;
    }


    public void setPassword(String password) {
        this.password = password;
    }



    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNumero_telephone(String numero_telephone) {
        this.numero_telephone = numero_telephone;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setRoles(Collection<RoleEnum> roles) {
        this.roles = roles;
    }

    public Collection<RoleEnum> getRoles() {
        return roles;
    }

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getEmail() {
        return email;
    }

    public String getNumero_telephone() {
        return numero_telephone;
    }

    public Set<EmpruntBean> getEmpruntBean() {
        return empruntBean;
    }

    public void setEmpruntBean(Set<EmpruntBean> empruntBean) {
        this.empruntBean = empruntBean;
    }


}

