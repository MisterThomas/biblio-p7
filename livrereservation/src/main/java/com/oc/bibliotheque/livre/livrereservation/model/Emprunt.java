package com.oc.bibliotheque.livre.livrereservation.model;

//import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "emprunt")
@Data
public class Emprunt {



    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "username")
    private String username;



    @Column(name = "emprunt_retour", nullable = false)
    private boolean emprunt_retour;



    @Column(name = "bonus_semaine")
    private boolean bonus_semaine;




    @Column(name = "date_emprunt")
    Date date_emprunt = new Date();


    @Column(name = "date_fin_prevu")
    String date_fin_prevu;

    @Column(name = "date_semaine_bonus")
    String date_semaine_bonus;




    public Emprunt() {
    }

    @ManyToOne
    @JoinColumn(name = "livre_id", nullable = true)
    private Livre livre;

    public Emprunt( String user, boolean bonus_semaine,boolean emprunt_retour, Date date_emprunt, String date_fin_prevu, String date_semaine_bonus, boolean isDone) {
        this.username = user;
        this.bonus_semaine = bonus_semaine;
        this.emprunt_retour = emprunt_retour;
        this.date_emprunt = date_emprunt;
        this.date_fin_prevu = date_fin_prevu;
        this.date_semaine_bonus = date_semaine_bonus;
    }

    public Livre getLivre() {
        return livre;
    }

    public void setLivre(Livre livre) {
        this.livre = livre;
    }


    @Override
        public String toString() {
            return "EmpruntBean{" +
                    "id=" + id +
                    ", username='" + username + '\'' +
                    ", auteur='" + getLivre().getAuteur() + '\'' +
                    ", titre='" + getLivre().getTitre() + '\'' +
                    ", emprunt_retour" + emprunt_retour +
                    ", bonus_semaine=" + bonus_semaine +
                    ", date_emprunt=" + date_emprunt +
                    ", date_fin_prevu='" + date_fin_prevu + '\'' +
                    ", date_semaine_bonus='" + date_semaine_bonus + '\'' +
                    ", livre=" + livre +
                    ", nombre_disponible=" + livre.getNombre_disponible() +
                    ", reservation_disponible=" + livre.isReservation_disponible() +
                    '}';
        };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isBonus_semaine() {
        return bonus_semaine;
    }

    public void setBonus_semaine(boolean bonus_semaine) {
        this.bonus_semaine = bonus_semaine;
    }


    public Date getDate_emprunt() {
        return date_emprunt;
    }

    public void setDate_emprunt(Date date_emprunt) {
        this.date_emprunt = date_emprunt;
    }

    public String getDate_fin_prevu() {
        return date_fin_prevu;
    }


    public void setDate_fin_prevu(String date_fin_prevu) {
        this.date_fin_prevu = date_fin_prevu;
    }


    public boolean isEmprunt_retour() {
        return emprunt_retour;
    }

    public void setEmprunt_retour(boolean emprunt_retour) {
        this.emprunt_retour = emprunt_retour;
    }

    public String getDate_semaine_bonus() {
        return date_semaine_bonus;
    }

    public void setDate_semaine_bonus(String date_semaine_bonus) {
        this.date_semaine_bonus = date_semaine_bonus;
    }


}
