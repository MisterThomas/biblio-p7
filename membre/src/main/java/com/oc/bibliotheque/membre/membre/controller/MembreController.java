package com.oc.bibliotheque.membre.membre.controller;


import com.oc.bibliotheque.membre.membre.model.Membre;
import com.oc.bibliotheque.membre.membre.repository.MembreRepository;
import com.oc.bibliotheque.membre.membre.service.MembreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class MembreController implements HealthIndicator {


    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MembreRepository membreRepository;



    @Autowired
    MembreService membreService;


    @Override
    public Health health() {
        List<Membre> membres = membreRepository.findAll();

        if(membres.isEmpty()) {
            return Health.down().build();
        }
        return Health.up().build();
    } //suite du code ... }



    @GetMapping("/api/listeMembres")
    public List<Membre> listeMembres(){
        log.info("liste Membres");
        List<Membre> membres = membreRepository.findAll();
        return membres;
    }




    @GetMapping("/api/membre/{username}")
    public Membre usernameMembre(@PathVariable("username") String username){
        log.info("membre username");
        return  membreService.findByUsername(username);
    }


    @GetMapping("/api/membre/compte/{id}")
    public Optional<Membre> compte(@PathVariable("id") long id) {
        log.info("membre id");
        return membreRepository.findById(id);
    }



}
