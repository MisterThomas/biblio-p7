package com.oc.bibliotheque.membre.membre.model;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.oc.bibliotheque.membre.membre.service.RoleEnum;
import lombok.Data;
import org.hibernate.annotations.Cascade;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Collections;

@Entity
@Table(name = "members")
@Data
public class Membre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "membre_id_pk")
    private Long id;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;


    @NotNull
    // @Size(min = 1, max = 30)
    @Column(name = "nom")
    private String nom;

    @NotNull
    //@Size(min = 1, max = 30)
    @Column(name = "prenom")
    private String prenom;


    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "email")
    private String email;


    // @NotNull
    // @Size(min = 10, max = 10)
    @Column(name = "numero_telephone")
    private String numero_telephone;



    @ElementCollection(targetClass = RoleEnum.class, fetch = FetchType.EAGER)
    @Cascade(value = org.hibernate.annotations.CascadeType.REMOVE)
    @JoinTable(
            indexes = {@Index(name = "INDEX_USER_ROLE", columnList = "id_membre")},
            name = "roles",
            joinColumns = @JoinColumn(name = "id_membre")
    )
    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)

    private Collection<RoleEnum> roles;






    public Membre() {
        this.roles = Collections.singletonList(RoleEnum.USER);
    }


    @JsonCreator
  public   Membre(@JsonProperty("id") final long id, @JsonProperty("username") final String username, @JsonProperty("password") final String password, @JsonProperty("nom")final String nom, @JsonProperty("prenom")final  String prenom, @JsonProperty("email") final String email, @JsonProperty("numero_telephone") final  String numero_telephone, @JsonProperty("role") final Collection<RoleEnum> roles
    ){
        this.id = id;
        this.username = username;
        this.password = password;
        this.nom = nom;
        this.prenom = prenom;
        this.numero_telephone = numero_telephone;
        this.email = email;
        this.roles = roles;

    }


    public String getPassword() {
        return password;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setNumero_telephone(String numero_telephone) {
        this.numero_telephone = numero_telephone;
    }

    public void setRoles(Collection<RoleEnum> roles) {
        this.roles = roles;
    }

}
