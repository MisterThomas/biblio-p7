package com.oc.bibliotheque.livre.livrereservation.controller;


import com.oc.bibliotheque.livre.livrereservation.beans.MembreBean;
import com.oc.bibliotheque.livre.livrereservation.model.Emprunt;
import com.oc.bibliotheque.livre.livrereservation.model.Livre;
import com.oc.bibliotheque.livre.livrereservation.proxies.MembreProxy;
import com.oc.bibliotheque.livre.livrereservation.repository.EmpruntRepository;
import com.oc.bibliotheque.livre.livrereservation.service.EmpruntService;
import com.oc.bibliotheque.livre.livrereservation.service.LivreService;
import com.oc.bibliotheque.livre.livrereservation.service.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@RestController
public class EmpruntController {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MailService notificationService;

    @Autowired
    MembreProxy membreProxy;

    @Autowired
    EmpruntRepository empruntRepository;

    @Autowired
    EmpruntService empruntService;

    @Autowired
    LivreService livreService;


    @GetMapping("/api/listeEmprunts")
    public List<Emprunt> listeEmprunts(){
        log.info("liste Emprunts");
        List<Emprunt> emprunts = empruntRepository.findAll();
        return emprunts;
    }


    @GetMapping("/api/listeEmpruntsEncours/username/{username}")
    public MembreBean membreBean(@PathVariable( "username") String username){
        log.info("mail-username by username");
        MembreBean membreBean = membreProxy.usernameMembre(username);

        return membreBean;
    }


    @GetMapping("/api/mail-username")
    public List<MembreBean> mail(){
        log.info("mail-username");
        List<MembreBean> membreBean = membreProxy.listeMembres();

        return membreBean;
    }


    @GetMapping("/api/listeEmpruntsEncours")
    public List<Emprunt> listeEmpruntsEncours(@RequestParam(value = "username", required=false) String username){
        log.info("liste Emprunts encours");
        List<Emprunt> emprunts = empruntService.getEmpruntByUsername(username);
        System.out.println(emprunts);
        return emprunts;
    }


    @GetMapping("/api/listeEmpruntsEncours/{username}")
    public List<Emprunt> listeEmpruntsEncoursUsername(@PathVariable( "username") String username){

        log.info("liste Emprunts encours username");
        List<Emprunt> emprunts = empruntService.getEmpruntByUsername(username);
        return emprunts;
    }

    @GetMapping("/api/listeEmpruntsEncours/id/{id}")
    public Optional<Emprunt> detailEmprunt(@PathVariable("id") long id){

        log.info("liste detailEmprunt");
        Optional<Emprunt> emprunt = empruntService.getEmpruntById(id);

        return emprunt;
    }


    @PutMapping("/api/listeEmpruntsEncours/upade-emprunt/{empruntId}")
    public Emprunt prolongerEmprunt(@PathVariable("empruntId") long empruntId) {

        log.info("prolonger emprunt");
        Emprunt emprunt = empruntService.getEmpruntById(empruntId).get();


        //   Date date = Calendar.getInstance().getTime();
        //   DateFormat dateFormat = new SimpleDateFormat("dd MM yyyy");
        //   Calendar calendar = Calendar.getInstance();
        //  calendar.setTime(date);
        //  date = calendar.getTime();


        if(emprunt.isBonus_semaine() == false && emprunt.isEmprunt_retour() == false){

            //  emprunt.setEmprunt_encours(true);
            emprunt.setBonus_semaine(true);

            //   emprunt.setDate_emprunt(date);
            //    calendar.add(Calendar.WEEK_OF_MONTH, 1);
            //   emprunt.setDate_fin_prevu(dateFormat.format(calendar.getTime()));



            return empruntService.updateEmprunt(emprunt);

        }
        return empruntService.updateEmprunt(emprunt);
    }


    @PutMapping("/api/listeEmpruntsEncours/retour-emprunt/{empruntId}")
    public Emprunt retourEmprunt(@PathVariable("empruntId") long empruntId) {

        log.info("retour emprunt");
        Emprunt emprunt = empruntService.getEmpruntById(empruntId).get();


        if(emprunt.isEmprunt_retour() == false){
            int total = 0;

            total = emprunt.getLivre().getNombre_disponible() + 1;

            emprunt.getLivre().setNombre_disponible(total);

            emprunt.setEmprunt_retour(true);

            //     livreService.saveLivre(emprunt.getLivre());
            return empruntService.retourEmprunt(emprunt) ;

        }
        return empruntService.retourEmprunt(emprunt);
    }

    @PostMapping("/api/{livreId}/listeEmprunts")
    Emprunt nouvelleEmprunt(@RequestBody @Valid Emprunt emprunt, @PathVariable("livreId") long id) throws ParseException {


        log.info("liste emprunt");
        Livre livre = livreService.getLivreById(id).get();
        emprunt.setLivre(livre);

        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        date = calendar.getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        date = calendar.getTime();

        if(emprunt.getLivre().getNombre_disponible() >= 0 ) {
            int total = 0;


            emprunt.setDate_emprunt(date);
            calendar.add(Calendar.WEEK_OF_MONTH, 4);
            emprunt.setDate_fin_prevu(dateFormat.format(calendar.getTime()));
            c.add(Calendar.WEEK_OF_MONTH, 5);
            emprunt.setDate_semaine_bonus(dateFormat.format(c.getTime()));


            total = emprunt.getLivre().getNombre_disponible() - 1;

            emprunt.getLivre().setNombre_disponible(total);

            emprunt = empruntService.retourEmprunt(emprunt);

            return emprunt;

        }
        return empruntService.retourEmprunt(emprunt);
    }


    @GetMapping("/api/send-mail")
    public String send() {



        log.info("send mail");
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String date_fin_prevu = dateFormat.format(date);


        Calendar calendar = Calendar.getInstance();



        List<Emprunt> emprunts = empruntRepository.findALLByAndDate_fin_prevu(date_fin_prevu);

        Iterator<Emprunt> iterEmprunt = emprunts.iterator();

        while (iterEmprunt.hasNext()) {

            Emprunt empruntBean = iterEmprunt.next();

            Date date1 = empruntBean.getDate_emprunt();
            Calendar c = Calendar.getInstance();
            c.setTime(date1);
            c.add(Calendar.WEEK_OF_MONTH, 3);
            date1 = c.getTime();


            if (calendar.getTime().compareTo(date1) >= 0 && (empruntBean.isEmprunt_retour() == false)) {
                MembreBean membreBean = membreProxy.usernameMembre(empruntBean.getUsername());
                membreBean.getEmail();
                //Receiver's email address
                try {
                    notificationService.sendEmail(membreBean);
                } catch (MailException mailException) {
                    System.out.println(mailException);
                }
                return "un mail de relance vient d'etre envoye a" + empruntBean.getUsername();
            } else if (empruntBean.isEmprunt_retour() == true) {
                return "livre rendu";
            }
        }
        //    }
        return "livre encours d'emprunt";
    }

}
