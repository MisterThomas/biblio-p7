package com.oc.bibliotheque.livre.livrereservation.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "livre")
@Data
public class Livre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "username")
    private String username;

    @Column(name = "auteur")
    private String auteur;

    @Column(name = "titre")
    private String titre;

    @Column(name = "nombre_disponible")
    private int  nombre_disponible;


    @Column(name = "reservation_disponible", nullable = false)
    private boolean reservation_disponible;


    public Livre() {
    }

    @JsonCreator
    public Livre(@JsonProperty("usernamlisteLivre") final String user,@JsonProperty("auteur") final String auteur, @JsonProperty("titre") final String titre, @JsonProperty("nombre_disponible")final int nombre_disponible, @JsonProperty("reservation_disponible") final boolean reservation_disponible, final boolean isDone) {
       // this.id = id;
        //@JsonProperty("id") final long id,
        this.username = user;
        this.auteur =auteur;
        this.titre = titre;
        this.nombre_disponible = nombre_disponible;
        this.reservation_disponible = reservation_disponible;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "livre", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private Set<Emprunt> emprunt = new HashSet<>();


    @Override
    public String toString() {
        return "LivreBean{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", auteur='" + auteur + '\'' +
                ", titre='" + titre + '\'' +
                ", nombre_disponible=" + nombre_disponible +
                ", reservation_disponible=" + reservation_disponible +
                ", empruntBean=" + emprunt +
                '}';
};

    public Set<Emprunt> getEmprunt() {
        return emprunt;
    }

    public void setEmprunt(Set<Emprunt> emprunt) {
        this.emprunt = emprunt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getNombre_disponible() {
        return nombre_disponible;
    }

    public void setNombre_disponible(int nombre_disponible) {
        this.nombre_disponible = nombre_disponible;
    }


    public boolean isReservation_disponible() {
        return reservation_disponible;
    }

    public void setReservation_disponible(boolean reservation_disponible) {
        this.reservation_disponible = reservation_disponible;
    }
}
