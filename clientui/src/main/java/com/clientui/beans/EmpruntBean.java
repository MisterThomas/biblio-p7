package com.clientui.beans;

import org.springframework.context.annotation.Bean;

import javax.persistence.*;

import java.util.Date;

@Entity
public class EmpruntBean {

    @Id
    private long id;

    private String userName;

    private boolean emprunt_retour;

    private boolean bonus_semaine;

    Date date_emprunt = new Date();

    String date_fin_prevu;

    String date_semaine_bonus;


    public EmpruntBean() {
    }

    public EmpruntBean(long id, String userName, boolean emprunt_retour, boolean bonus_semaine, Date date_emprunt, String date_fin_prevu, String date_semaine_bonus, LivreBean livre, MembreBean membreBean) {
        this.id = id;
        this.userName = userName;
        this.emprunt_retour = emprunt_retour;
        this.bonus_semaine = bonus_semaine;
        this.date_emprunt = date_emprunt;
        this.date_fin_prevu = date_fin_prevu;
        this.date_semaine_bonus = date_semaine_bonus;
        this.livre = livre;
        this.membreBean = membreBean;
    }

    @ManyToOne
    @JoinColumn(name = "livre_id", nullable = false)
    private LivreBean livre;

    public LivreBean getLivre() {
        return livre;
    }

    public void setLivre(LivreBean livre) {
        this.livre = livre;
    }


    @ManyToOne
    @JoinColumn(name = "membre_id", nullable = false)
    private MembreBean membreBean;

    public MembreBean getMembreBean() {
        return membreBean;
    }

    public void setMembreBean(MembreBean membreBean) {
        this.membreBean = membreBean;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isEmprunt_retour() {
        return emprunt_retour;
    }

    public void setEmprunt_retour(boolean emprunt_retour) {
        this.emprunt_retour = emprunt_retour;
    }

    public boolean isBonus_semaine() {
        return bonus_semaine;
    }

    public void setBonus_semaine(boolean bonus_semaine) {
        this.bonus_semaine = bonus_semaine;
    }

    public Date getDate_emprunt() {
        return date_emprunt;
    }

    public void setDate_emprunt(Date date_emprunt) {
        this.date_emprunt = date_emprunt;
    }

    public String getDate_fin_prevu() {
        return date_fin_prevu;
    }

    public void setDate_fin_prevu(String date_fin_prevu) {
        this.date_fin_prevu = date_fin_prevu;
    }

    public String getDate_semaine_bonus() {
        return date_semaine_bonus;
    }

    public void setDate_semaine_bonus(String date_semaine_bonus) {
        this.date_semaine_bonus = date_semaine_bonus;
    }

    @Override
    public String toString() {
        return "EmpruntBean{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", auteur='" + livre.getAuteur() + '\'' +
                ", titre='" + livre.getTitre() + '\'' +
                ", bonus_semaine=" + bonus_semaine +
                ", date_emprunt=" + date_emprunt +
                ", date_fin_prevu='" + date_fin_prevu + '\'' +
                ", date_semaine_bonus='" + date_semaine_bonus + '\'' +
                ", livre=" + livre +
                ", membreBean=" + membreBean +
                '}';
    }

}
