package com.oc.bibliotheque.membre.membre.service;

import com.oc.bibliotheque.membre.membre.model.Membre;
import com.oc.bibliotheque.membre.membre.repository.MembreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;


@Service("MembreService")
public class MembreService  {

            @Autowired
            private final MembreRepository membreRepository;

            @Autowired
            private static BCryptPasswordEncoder passwordEncoder;

            @Autowired
            public MembreService(MembreRepository membreRepository) {
             this.membreRepository = membreRepository;
            }




    public Membre save(Membre membre) {
        membre.setPassword(passwordEncoder.encode(membre.getPassword()));
        Collection<RoleEnum> collection = new HashSet<RoleEnum>();
        collection.add(RoleEnum.USER);
        membre.setRoles(collection);
        membreRepository.save(membre);

        return membre;
    }


    public Membre findByUsername(String username) {
        return membreRepository.findByUsername(username).get(0);
    }

            public List<Membre> getMembreByUsername(String username){
                return  membreRepository.findByUsername(username);
            }


}
