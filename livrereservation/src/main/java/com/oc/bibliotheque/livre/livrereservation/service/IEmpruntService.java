package com.oc.bibliotheque.livre.livrereservation.service;

import com.oc.bibliotheque.livre.livrereservation.model.Emprunt;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IEmpruntService {


    List<Emprunt> getEmpruntByUsername(String emprunt);


    Optional<Emprunt> getEmpruntById(long id);

    void addEmprunt(String username, boolean bonus_semaine,boolean emprunt_retour, Date date_emprunt, String date_fin_prevu, String date_semaine_bonus, boolean isDone);

    Emprunt updateEmprunt(Emprunt emprunt);

    Emprunt saveEmprunt(Emprunt emprunt);

    Emprunt retourEmprunt(Emprunt emprunt);



}
