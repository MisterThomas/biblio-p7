package com.oc.bibliotheque.livre.livrereservation.service;


import com.oc.bibliotheque.livre.livrereservation.beans.MembreBean;
import com.oc.bibliotheque.livre.livrereservation.model.Emprunt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
//import javax.mail.MessagingException;
//import javax.mail.internet.MimeMessage;



@Service
public class MailService {

    @Autowired
    private JavaMailSender javaMailSender;





    @Autowired
    public MailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }


    public void sendEmail(MembreBean membreBean) throws MailException {

        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setTo(membreBean.getEmail());
        mail.setSubject("Retour de votre livre");
        mail.setText("Bonjour vous avez un emprunt encours qui fait bientot rendre");


    javaMailSender.send(mail);



    }


/*
    public void sendEmailWithAttachment(MembreBean membreBean) throws MailException, MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

        helper.setTo(membreBean.getEmail());
        helper.setSubject("Retour de votre livre");
        helper.setText("Bonjour vous avez prolonger l'emprunt. vous nous rendez quand le livre ?");

        ClassPathResource classPathResource = new ClassPathResource("Livre.pdf");
        helper.addAttachment(classPathResource.getFilename(), classPathResource);

        javaMailSender.send(mimeMessage);
    }
*/


}
