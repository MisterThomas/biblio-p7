package com.clientui.validator;

        import com.clientui.beans.MembreBean;
        import org.springframework.stereotype.Component;
        import org.springframework.validation.Errors;
        import org.springframework.validation.Validator;

@Component
public class MembreValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return MembreBean.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        MembreBean membre = (MembreBean) o;
    }
}
